using System;
using UnityEngine;

namespace Main
{
    public class MainManager : MonoBehaviour
    {
        [SerializeField] private GameObject gamePrefab;

        private void Awake()
        {
            // LoadGame();
        }

       private void LoadGame()
       {
           if (gamePrefab == null)
           {
               Debug.LogWarning("MainManager: gamePrefab missing");
               return;
           }
               
           Instantiate(gamePrefab, transform);
        }
    }
}