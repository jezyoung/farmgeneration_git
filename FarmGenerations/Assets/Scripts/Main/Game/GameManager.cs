using Main.Game.GameUI;
using Main.Game.Input;
using Main.Game.TurnManagement;
using Main.Game.World;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Main.Game
{
    public class GameManager : MonoBehaviour
    {
        private InputManager _inputManager;
        private TurnManager _turnManager;
        private WorldManager _worldManager;
        private TilePopupMenu _tilePopupMenu;

        private void Awake()
        {
            _inputManager = GetComponentInChildren<InputManager>();
            _turnManager = GetComponentInChildren<TurnManager>();
            _worldManager = GetComponentInChildren<WorldManager>();
            _tilePopupMenu = GetComponentInChildren<TilePopupMenu>();
            
            _inputManager.mouseClick += OnMouseClick;
            _tilePopupMenu.moveButtonPressed += OnMoveButtonPressed;
        }

        void OnMouseClick(Vector3 worldPoint)
        {
            Color newColor = Color.black;
            Vector3Int cellIndex = _worldManager.tileMap.WorldToCell(worldPoint);
            print(worldPoint);
            print(cellIndex);
            string tileName = _worldManager.tileMap.GetTile(cellIndex).name;
            print(tileName);

            _tilePopupMenu.ShowMenu(cellIndex);
            // switch (tileName)
            // {
            //     case "Grass":
            //         print("you clicked on grass!");
            //         break;
            //     case "Forrest":
            //         print("you clicked on forrest!");
            //         break;
            // }
        }

        void OnMoveButtonPressed()
        {
            
        }
    }
}