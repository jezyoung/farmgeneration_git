using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace Main.Game.Input
{
    public class InputManager : MonoBehaviour
    {
        public delegate void MouseClick(Vector3 mousePos);
        public MouseClick mouseClick;
        
        public Vector2 clickPosition;

        private void Start()
        {
            // mouseClick();
        }

        private void Update()
        {
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                if (!IsMouseOverUi())
                {
                    Ray ray = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
                    Vector3 worldPoint = ray.GetPoint(-ray.origin.z / ray.direction.z);
                    mouseClick(worldPoint);
                }

                else
                {
                    print(IsMouseOverUi());
                }
                
            }
            
            

        }

        private bool IsMouseOverUi()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }
    }
}