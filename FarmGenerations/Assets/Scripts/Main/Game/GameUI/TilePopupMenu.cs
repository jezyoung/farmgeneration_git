using System;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Main.Game.GameUI
{
    public class TilePopupMenu : MonoBehaviour
    {
        public delegate void MoveButtonPressed();
        public MoveButtonPressed moveButtonPressed;
        
        [SerializeField] private Button moveButton;
        [SerializeField] private Transform menu;

        private void Awake()
        {
            moveButton.onClick.AddListener(OnButtonClick);
        }

        private void Update()
        {
            
        }

        void OnButtonClick()
        {
            moveButtonPressed();
            
        }
        
        public void ShowMenu(Vector3 mousePos)
        {
            print("show menu at " + mousePos);
            menu.position = mousePos;
        }
    }
}