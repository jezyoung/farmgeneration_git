using UnityEngine;

namespace Main.Game.World.Characters
{
    public abstract class Character: MonoBehaviour
    {
        public void Move(Vector3 nextPos)
        {
            LeanTween.move(gameObject, nextPos, 0.5f).setEaseInExpo();
        }
    }
}