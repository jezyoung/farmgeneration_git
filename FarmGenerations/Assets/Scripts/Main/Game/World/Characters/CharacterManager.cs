using System;
using System.Collections.Generic;
using Main.Game.World.Characters;
using UnityEngine;

namespace Characters
{
    public class CharacterManager : MonoBehaviour
    {
        public List<Character> Characters = new List<Character>();

        private void Start()
        {
            foreach (var character in Characters)
            {
               character.Move(Vector3.forward);
               
            }
        }
    }
}